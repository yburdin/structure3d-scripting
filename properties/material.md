# Работа с материалами

- [Добавить новый материал](#добавить-новый-материал)
  - [addMaterial](#addmaterial)
  - [addMaterialFromDatabase](#addmaterialfromdatabase)
- [Получить информацию о материале](#получить-информацию-о-материале)
  - [listMaterials](#listmaterials)
  - [getMaterialName](#getmaterialname)
  - [getMaterialProperties](#getmaterialproperties)
- [Редактировать материал](#редактировать-материал)
  - [setMaterialName](#setmaterialname)
  - [setMaterialProperties](#setmaterialproperties)
- [Взаимодействие с элементами](#взаимодействие-с-элементами)
  - [setElementsMaterial](#setelementsmaterial)
  - [getMaterialByElementId](#getmaterialbyelementid)
  - [getElementsByMaterialId](#getelementsbymaterialid)
  - [getElementsWithoutMaterial](#getelementswithoutmaterial)
- [Параметры материала](#параметры-материала)

## Добавить новый материал

### addMaterial

```python
addMaterial(
  name: str,
  properties: list[MaterialProperty],
) -> int
```

#### Описание

Функция добавляет новый материал в модель с заданными свойствами
и возвращает его индекс.

#### Параметры

- name (str): Название нового материала.
- properties (list[MaterialProperty]): Список свойств материала.

#### Возвращаемое значение

- int: Индекс нового материала.

### addMaterialFromDatabase

```python
addMaterialFromDatabase(
  database: Database,
  materialId: int,
) -> int
```

#### Описание

Функция добавляет новый материал в модель из базы данных
по индексу материала в базе и возвращает его индекс в модели.

#### Параметры

- database (Database): База данных, из которой необходимо получить материал.
- materialId (int): Индекс материала в базе данных.

#### Возвращаемое значение

- int: Индекс нового материала.

## Получить информацию о материале

### listMaterials

```python
listMaterials(
) -> list[int]
```

#### Описание

Функция возвращает список доступных материалов.

#### Возвращаемое значение

- list[int]: Список индексов материалов.

### getMaterialName

```python
getMaterialName(
  materialId: int,
) -> str
```

#### Описание

Функция возвращает название материала по его индексу.

#### Параметры

- materialId (int): Индекс материала.

#### Возвращаемое значение

- str: Название материала.

### getMaterialProperties

```python
getMaterialProperties(
  materialId: int,
) -> list[MaterialProperty]
```

#### Описание

Функция возвращает список свойств материала по его индексу.

#### Параметры

- materialId (int): Индекс материала.

#### Возвращаемое значение

- list[MaterialProperty]: Список свойств материала.

## Редактировать материал

### setMaterialName

```python
setMaterialName(
  materialId: int,
  name: str,
) -> None
```

#### Описание

Функция изменяет название материала по его индексу.

#### Параметры

- materialId (int): Индекс материала, который необходимо изменить.
- name (str): Новое название материала.

### setMaterialProperties

```python
setMaterialProperties(
  materialId: int,
  properties: list[MaterialProperty],
) -> None
```

#### Описание

Функция изменяет свойства материала по его индексу.

#### Параметры

- materialId (int): Индекс материала, у которого необходимо изменить свойства.
- properties (list[MaterialProperty]): Новый список свойств материала.

## Взаимодействие с элементами

### setElementsMaterial

```python
setElementsMaterial(
  materialId: int,
  elements: list[tuple[Enum, int]],
) -> None
```

#### Описание

Функция назначает указанным элементам заданный материал.

#### Параметры

- materialId (int): Индекс материала, который необходимо присвоить элементу.
- elements (list[tuple[Enum, int]]): Список элементов,
которым необходимо назначить материал.
Каждый элемент представляется в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.

### getMaterialByElementId

```python
getMaterialByElementId(
  element: tuple[Enum, int],
) -> int
```

#### Описание

Функция возвращает индекс материала, связанного с указанным элементом.

#### Параметры

- element (tuple[Enum, int]): Элемент в формате кортежа,
состоящего из перечисления (Enum) и индекса элемента.

#### Возвращаемое значение

- int: Индекс материала, связанного с указанным элементом.

### getElementsByMaterialId

```python
getElementsByMaterialId(
  materialId: int,
) -> list[tuple[Enum, int]]
```

#### Описание

Функция возвращает список индексов элементов, связанных с указанным материалом.

#### Параметры

- materialId (int): Индекс материала.

#### Возвращаемое значение

- list[tuple[Enum, int]]: Список элементов.
Каждый элемент представлен в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.

### getElementsWithoutMaterial

```python
getElementsWithoutMaterial(
) -> list[tuple[Enum, int]]
```

#### Описание

Функция возвращает список индексов элементов,
которые не имеют привязки к какому-либо материалу.

#### Возвращаемое значение

- list[tuple[Enum, int]]: Список элементов.
Каждый элемент представлен в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.

## Параметры материала

```python
class MaterialProperty:
  ...
```

```python
class GeneralProperty(MaterialProperty):
  yieldStrength: float
  ultimateStrength: float
```

```python
class IsotropicProperty(MaterialProperty):
  youngsModulus: float
  poissonCoefficient: float
  density: float
  thermalExpansionCoefficient: float  
```
