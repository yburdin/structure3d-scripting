# Работа с сечениями

- [Добавить новое сечение](#добавить-новое-сечение)
  - [addSectionFromLibrary](#addsectionfromlibrary)
  - [addParametricSection](#addparametricsection)
- [Получить информацию о сечении](#получить-информацию-о-сечении)
  - [listSections](#listsections)
  - [getSectionName](#getsectionname)
  - [getSectionProperties](#getsectionproperties)
- [Редактировать сечение](#редактировать-сечение)
  - [setSectionName](#setsectionname)
- [Взаимодействие с элементами](#взаимодействие-с-элементами)
  - [setRodsSection](#setrodssection)
  - [getSectionByRodId](#getsectionbyrodid)
  - [getRodsBySectionId](#getrodsbysectionid)
  - [getRodsWithoutSection](#getrodswithoutsection)

## Добавить новое сечение

### addSectionFromLibrary

```python
addSectionFromLibrary(
  library: Path,
  sectionId: int,
) -> int
```

#### Описание

Функция добавляет новое сечение из библиотеку и возвращает его индекс.

#### Параметры

- library (Path): Путь к библиотеке, из которой нужно добавить сечение.
- sectionId (int): Индекс сечения в библиотеке.

#### Возвращаемое значение

- int: Индекс добавленного сечения.

### addParametricSection

```python
addParametricSection(
  parametricSectionId: int,
  **parameters,
) -> int
```

#### Описание

Функция добавляет новое параметрическое сечение и возвращает его индекс.

#### Параметры

- parametricSectionId (int): Индекс параметрического сечения.
- parameters: Параметры для создания параметрического сечения.

#### Возвращаемое значение

- int: Индекс добавленного сечения.

## Получить информацию о сечении

### listSections

```python
listSections(
) -> list[int]
```

#### Описание

Функция возвращает список доступных сечений.

#### Возвращаемое значение

- list[int]: Список индексов сечений.

### getSectionName

```python
getSectionName(
  sectionId: int,
) -> str
```

#### Описание

Функция возвращает имя сечения по его индексу.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- str: Имя сечения.

### getSectionProperties

```python
getSectionProperties(
  sectionId: int,
) -> SectionProperties
```

#### Описание

Функция возвращает свойства сечения по его индексу.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- SectionProperties: Свойства сечения.

## Редактировать сечение

### setSectionName

```python
setSectionName(
  sectionId: int,
  name: str,
) -> None
```

#### Описание

Функция изменяет имя сечения по его индексу.

#### Параметры

- sectionId (int): Индекс сечения.
- name (str): Новое имя для сечения.

## Взаимодействие с элементами

### setRodsSection

```python
setRodsSection(
  sectionId: int,
  rods: list[int],
) -> None
```

#### Описание

Функция задает указанное сечение для указанных стержней.

#### Параметры

- sectionId (int): Индекс сечения.
- rods (list[int]): Список индексов стержней.

### getSectionByRodId

```python
getSectionByRodId(
  rodId: int,
) -> int
```

#### Описание

Функция возвращает индекс сечения заданного стержня.

#### Параметры

- rodId (int): Индекс стержня.

#### Возвращаемое значение

- int: Индекс сечения.

### getRodsBySectionId

```python
getRodsBySectionId(
  sectionId: int,
) -> list[int]
```

#### Описание

Функция возвращает список индексов всех стержней с указанным сечением.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- list[int]: Список индексов стержней.

### getRodsWithoutSection

```python
getRodsWithoutSection(
) -> list[int]
```

#### Описание

Функция возвращает список индексов всех стержней, которым не задано сечение.

#### Возвращаемое значение

- list[int]: Список индексов стержней.
