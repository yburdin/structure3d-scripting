# Работа с группами выделения

- [Создать новую группу выделения](#создать-новую-группу-выделения)
  - [addSelection](#addselection)
- [Получить информацию о группе выделения](#получить-информацию-о-группе-выделения)
  - [listSelections](#listselections)
  - [getSelectionName](#getselectionname)
- [Редактировать группу выделения](#редактировать-группу-выделения)
  - [setSelectionName](#setselectionname)
- [Взаимодействие с элементами](#взаимодействие-с-элементами)
  - [addObjectsToSelection](#addobjectstoselection)
  - [removeObjectsFromSelection](#removeobjectsfromselection)
  - [getSelectionsByObjectId](#getselectionsbyobjectid)
  - [getObjectsBySelectionId](#getobjectsbyselectionid)

## Создать новую группу выделения

### addSelection

```python
addSelection(
  name: str,
) -> int
```

#### Описание

Функция создает новую группу выделения и возвращает ее индекс.

#### Параметры

- name (str): Название новой группы выделения.

#### Возвращаемое значение

- int: Индекс новой группы выделения.

## Получить информацию о группе выделения

### listSelections

```python
listSelections(
) -> list[int]
```

#### Описание

Функция возвращает список доступных групп выделения.

#### Возвращаемое значение

- list[int]: Список индексов групп выделения.

### getSelectionName

```python
getSelectionName(
  selectionId: int,
) -> str
```

#### Описание

Функция возвращает название группы выделения по ее индексу.

#### Параметры

- selectionId (int): Индекс группы выделения.

#### Возвращаемое значение

- str: Название группы выделения.

## Редактировать группу выделения

### setSelectionName

```python
setSelectionName(
  selectionId: int
  name: str,
) -> None
```

#### Описание

Функция изменяет название группы выделения по ее индексу.

#### Параметры

- selectionId (int): Индекс группы выделения, которую необходимо изменить.
- name (str): Новое название группы выделения.

## Взаимодействие с элементами

### addObjectsToSelection

```python
addObjectsToSelection(
  objects: list[tuple[Enum, int]],
  selectionId: int,
) -> None
```

#### Описание

Функция добавляет элементы в указанную группу выделения.

#### Параметры

- objects (list[tuple[Enum, int]]): Список элементов, которые необходимо добавить.
Каждый элемент представляется в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.
- selectionId (int): Индекс группы выделения, к которой необходимо добавить элементы.

### removeObjectsFromSelection

```python
removeObjectsFromSelection(
  objects: list[tuple[Enum, int]],
  selectionId: int,
) -> None
```

#### Описание

Функция удаляет элементы из указанной группы выделения.

#### Параметры

- objects (list[tuple[Enum, int]]): Список элементов, которые необходимо удалить.
Каждый элемент представляется в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.
- selectionId (int): Индекс группы выделения, из которой необходимо удалить элементы.

### getSelectionsByObjectId

```python
getSelectionsByObjectId(
  objectId: tuple[Enum, int],
) -> list[int]
```

#### Описание

Функция возвращает список индексов групп выделения, которые содержат указанный элемент.

#### Параметры

- objectId (tuple[Enum, int]): Индекс элемента,
для которого необходимо найти соответствующие группы выделения.
Элемент представлен в виде кортежа, содержащего перечисление (Enum) и индекс элемента.

#### Возвращаемое значение

- list[int]: Список индексов групп выделения, содержащих указанный элемент.

### getObjectsBySelectionId

```python
getObjectsBySelectionId(
  selectionId: int,
) -> list[tuple[Enum, int]]
```

#### Описание

Функция возвращает список элементов, содержащихся в указанной группе выделения.

#### Параметры

- selectionId (int): Индекс группы выделения,
для которой необходимо получить список элементов.

#### Возвращаемое значение

- list[tuple[Enum, int]]: Список элементов,
содержащихся в указанной группе выделения.
Каждый элемент представлен в виде кортежа,
содержащего перечисление (Enum) и индекс элемента.
