# Работа с загружениями и комбинациями

- [Создание загружения и комбинации](#создание-загружения-и-комбинации)
  - [addLoadCase](#addloadcase)
  - [addDynamicLoadCase](#adddynamicloadcase)
  - [duplicateLoadCase](#duplicateloadcase)
  - [addLoadCaseCombination](#addloadcasecombination)
  - [duplicateLoadCaseCombination](#duplicateloadcasecombination)
- [Получение информации о загружении и комбинации](#получение-информации-о-загружении-и-комбинации)
  - [listLoadCases](#listloadcases)
  - [listDynamicLoadCases](#listdynamicloadcases)
  - [listLoadCaseCombinations](#listloadcasecombinations)
  - [getLoadCase](#getloadcase)
  - [getLoadCaseCombination](#getloadcasecombination)
- [Редактирование загружения и комбинации](#редактирование-загружения-и-комбинации)
  - [setName](#setname)
  - [addLoadCaseToCombination](#addloadcasetocombination)
  - [changeLoadCaseMultiplier](#changeloadcasemultiplier)
  - [deleteLoadCaseFromCombination](#deleteloadcasefromcombination)
  - [deleteLoadCase](#deleteloadcase)

## Создание загружения и комбинации

### addLoadCase

```python
addLoadCase(
  analysisId: int,
) -> int
```

#### Описание

Функция создает новое загружение в анализе.

#### Параметры

- analysisId (int): Индекс анализа, к которому добавляется загружение.

#### Возвращаемое значение

- int: Индекс нового загружения.

### addDynamicLoadCase

```python
addDynamicLoadCase(
  analysisId: int,
) -> int
```

#### Описание

Функция создает новое динамическое загружение в анализе.

#### Параметры

- analysisId (int): Индекс анализа, к которому добавляется динамическое загружение.

#### Возвращаемое значение

- int: Индекс нового динамического загружения.

### duplicateLoadCase

```python
duplicateLoadCase(
  loadCaseId: int,
) -> int
```

#### Описание

Функция создает копию существующего загружения.

#### Параметры

- loadCaseId (int): Индекс существующего загружения, которое нужно скопировать.

#### Возвращаемое значение

- int: Индекс нового загружения, являющегося копией существующего загружения.

### addLoadCaseCombination

```python
addLoadCaseCombination(
  analysisId: int,
) -> int
```

#### Описание

Функция создает новую комбинацию загружений в анализе.

#### Параметры

- analysisId (int): Индекс анализа, к которому добавляется комбинация загружений.

#### Возвращаемое значение

- int: Индекс новой комбинации загружений.

### duplicateLoadCaseCombination

```python
duplicateLoadCaseCombination(
  loadCaseCombinationId: int,
) -> int
```

#### Описание

Функция создает копию существующей комбинации загружений.

#### Параметры

- loadCaseCombinationId (int): Индекс существующей комбинации загружений,
которую нужно скопировать.

#### Возвращаемое значение

- int: Индекс новой комбинации загружений,
являющейся копией существующей комбинации загружений.

## Получение информации о загружении и комбинации

### listLoadCases

```python
listLoadCases(
) -> list[int]
```

#### Описание

Функция возвращает список индексов всех загружений.

#### Возвращаемое значение

- list[int]: Список индексов загружений.

### listDynamicLoadCases

```python
listDynamicLoadCases(
) -> list[int]
```

#### Описание

Функция возвращает список индексов всех динамических загружений.

#### Возвращаемое значение

- list[int]: Список индексов динамических загружений.

### listLoadCaseCombinations

```python
listLoadCaseCombinations(
) -> list[int]
```

#### Описание

Функция возвращает список индексов всех комбинаций загружений.

#### Возвращаемое значение

- list[int]: Список индексов комбинаций загружений.

### getLoadCase

```python
getLoadCase(
  loadCaseId: int,
) -> LoadCase
```

#### Описание

Функция возвращает информацию о конкретном загружении.

#### Параметры

- loadCaseId (int): Индекс загружения, информацию о котором нужно получить.

#### Возвращаемое значение

- LoadCase: Объект, содержащий информацию о загружении.

### getLoadCaseCombination

```python
getLoadCaseCombination(
  loadCaseCombinationId: int,
) -> LoadCaseCombination
```

#### Описание

Функция возвращает информацию о конкретной комбинации загружений.

#### Параметры

- loadCaseCombinationId (int): Индекс комбинации загружений,
информацию о которой нужно получить.

#### Возвращаемое значение

- LoadCaseCombination: Объект, содержащий информацию о комбинации загружений.

## Редактирование загружения и комбинации

### setName

```python
setName(
  loadCaseId: int | loadCaseCombinationId: int,
  name: str,
) -> None
```

#### Описание

Функция устанавливает новое имя для загружения или комбинации загружений.

#### Параметры

- loadCaseId (int) | loadCaseCombinationId (int):
Индекс загружения или комбинации загружений,
для которого нужно изменить имя.
- name (str): Новое имя.

### addLoadCaseToCombination

```python
addLoadCaseToCombination(
  loadCaseId: int,
  loadCaseCombinationId: int,
  multiplier: float,
) -> None
```

#### Описание

Функция добавляет загружение к существующей комбинации загружений с указанным множителем.

#### Параметры

- loadCaseId (int): Индекс загружения, которое нужно добавить к комбинации загружений.
- loadCaseCombinationId (int): Индекс комбинации загружений,
к которой нужно добавить загружение.
- multiplier (float): Множитель для указанного загружения в комбинации загружений.

### changeLoadCaseMultiplier

```python
changeLoadCaseMultiplier(
  loadCaseId: int,
  loadCaseCombinationId: int,
  multiplier: float,
) -> None
```

#### Описание

Функция изменяет множитель для указанного загружения в комбинации загружений.

#### Параметры

- loadCaseId (int): Индекс загружения,
для которого нужно изменить множитель в комбинации загружений.
- loadCaseCombinationId (int): Индекс комбинации загружений,
в которой нужно изменить множитель для указанного загружения.
- multiplier (float): Новый множитель для указанного загружения в комбинации загружений.

### deleteLoadCaseFromCombination

```python
deleteLoadCaseFromCombination(
  loadCaseId: int,
  loadCaseCombinationId: int,
) -> None
```

#### Описание

Функция удаляет загружение из комбинации загружений.

#### Параметры

- loadCaseId (int): Индекс загружения, которое нужно удалить из комбинации загружений.
- loadCaseCombinationId (int): Индекс комбинации загружений,
из которой нужно удалить загружение.

### deleteLoadCase

```python
deleteLoadCase(
  loadCaseId: int | loadCaseCombinationId: int,
) -> None
```

#### Описание

Функция удаляет загружение или комбинацию загружений.

#### Параметры

- loadCaseId (int) | loadCaseCombinationId (int):
Индекс загружения или комбинации загружений, которую нужно удалить.
