# Работа с опорами

- [Создание опоры](#создание-опоры)
  - [addRigidSupport](#addrigidsupport)
  - [addElasticSupport](#addelasticsupport)
- [Получение информации об опоре](#получение-информации-об-опоре)
  - [listSupports](#listsupports)
  - [getSupport](#getsupport)
- [Редактирование опоры](#редактирование-опоры)
  - [setName](#setname)
  - [editRigidSupport](#editrigidsupport)
  - [editElasticSupport](#editelasticsupport)
  - [deleteSupport](#deletesupport)

## Создание опоры

### addRigidSupport

```python
addRigidSupport(
  analysisId: int,
  nodes: list[int],
  components: RigidSupportComponents
) -> int
```

#### Описание

Функция используется для создания жесткой опоры в анализе.

#### Параметры

- analysisId (int): Индекс анализа, к которому добавляется опора.
- nodes (list[int]): Список индексов узлов, к которым применяется опора.
- components (RigidSupportComponents): Компоненты жесткой опоры,
определяющие ограничение перемещения в каждом направлении.

#### Возвращаемое значение

- int: Индекс созданной опоры.

### addElasticSupport

```python
addElasticSupport(
  analysisId: int,
  nodes: list[int],
  stiffness: ElasticSupportStiffness,
) -> int
```

#### Описание

Функция используется для создания упругой опоры в анализе.

#### Параметры

- analysisId (int): Индекс анализа, к которому добавляется опора.
- nodes (list[int]): Список индексов узлов, к которым применяется опора.
- stiffness (ElasticSupportStiffness): Коэффициенты жесткости упругой опоры.

#### Возвращаемое значение

- int: Индекс созданной опоры.

## Получение информации об опоре

### listSupports

```python
listSupports(
  analysisId: int,
) -> list[int]
```

#### Описание

Функция используется для получения списка индексов опор,
примененных в указанном анализе.

#### Параметры

- analysisId (int): Индекс анализа, для которого требуется получить список опор.

#### Возвращаемое значение

- list[int]: Список индексов опор.

### getSupport

```python
getSupport(
  supportId: int,
) -> Support
```

#### Описание

Функция используется для получения подробной информации о
конкретной опоре по её индексу.

#### Параметры

- supportId (int): Индекс опоры, для которой требуется получить информацию.

#### Возвращаемое значение

- Support: Объект, содержащий информацию об опоре.

## Редактирование опоры

### setName

```python
setName(
  supportId: int,
  name: str,
) -> None
```

#### Описание

Функция используется для изменения имени опоры по её индексу.

#### Параметры

- supportId (int): Индекс опоры, для которой требуется изменить имя.
- name (str): Новое имя опоры.

### editRigidSupport

```python
editRigidSupport(
  supportId: int,
  newNodes: list[int] | None,
  newComponents: RigidSupportComponents | None,
) -> None
```

#### Описание

Функция используется для редактирования параметров жесткой опоры по её индексу.

#### Параметры

- supportId (int): Индекс жесткой опоры, которую требуется отредактировать.
- newNodes (list[int] | None): Новый список индексов узлов,
к которым применяется опора.
Если значение равно `None`, то узлы не изменяются.
- newComponents (RigidSupportComponents | None): Новые компоненты жесткой опоры,
определяющие ограничение движения в каждом направлении.
Если значение равно `None`, то компоненты не изменяются.

### editElasticSupport

```python
editElasticSupport(
  supportId: int,
  newNodes: list[int] | None,
  newStiffness: ElasticSupportStiffness | None,
) -> None
```

#### Описание

Функция используется для редактирования параметров упругой опоры по её индексу.

#### Параметры

- supportId (int): Индекс упругой опоры, которую требуется отредактировать.
- newNodes (list[int] | None): Новый список индексов узлов,
к которым применяется опора.
Если значение равно `None`, то узлы не изменяются.
- newStiffness (ElasticSupportStiffness | None):
Новые коэффициенты жесткости упругой опоры,
определяющие поведение конструкции в каждом направлении.
Если значение равно `None`, то коэффициенты не изменяются.

### deleteSupport

```python
deleteSupport(
  supportId: int,
) -> None
```

#### Описание

Функция используется для удаления опоры по её индексу.

#### Параметры

- supportId (int): Индекс опоры, которую требуется удалить.
