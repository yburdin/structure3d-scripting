# Получение информации о модели

- [Операции получения списка элементов](#операции-получения-списка-элементов)
  - [listElements](#listelements)
  - [listNodes](#listnodes)
  - [listPointMasses](#listpointmasses)
  - [listRods](#listrods)
  - [listElasticLinks](#listelasticlinks)
- [Получение информации об освобождении связи](#получение-информации-об-освобождении-связи)
  - [getNodeReleases](#getnodereleases)
  - [getRodReleases](#getrodreleases)
  - [getNodeRelease](#getnoderelease)
  - [getRodRelease](#getrodrelease)

## Операции получения списка элементов

### listElements

```python
listElements(
) -> list[tuple[Enum, int]]
```

#### Описание

Функция возвращает список элементов.
Каждый элемент представлен кортежем, содержащим перечисление (Enum) и индекс элемента.

#### Возвращаемое значение

list[tuple[Enum, int]]: Список кортежей,
содержащих перечисления (Enum) и индексы элементов.

### listNodes

```python
listNodes(
) -> list[int]
```

#### Описание

Функция возвращает список узлов.

#### Возвращаемое значение

- list[int]: Список индексов узлов.

### listPointMasses

```python
listPointMasses(
) -> list[int]
```

#### Описание

Функция возвращает список точечных масс.

#### Возвращаемое значение

- list[int]: Список индексов точечных масс.

### listRods

```python
listRods(
) -> list[int]
```

#### Описание

Функция возвращает список стержней.

#### Возвращаемое значение

- list[int]: Список индексов стержней.

### listElasticLinks

```python
listElasticLinks(
) -> list[int]
```

#### Описание

Функция возвращает список упругих связей (эластичных связей).

#### Возвращаемое значение

- list[int]: Список индексов упругих связей.

## Получение информации об освобождении связи

### getNodeReleases

```python
getNodeReleases(
) -> list[NodeRelease]
```

#### Описание

Функция возвращает список шарниров в узлах.

#### Возвращаемое значение

- list[NodeRelease]: Список шарниров.

### getRodReleases

```python
getRodReleases(
) -> list[RodRelease]
```

#### Описание

Функция возвращает список освобождений связей стержней.

#### Возвращаемое значение

- list[RodRelease]: Список освобождений связей стержней.

### getNodeRelease

```python
getNodeRelease(
  nodeId: int,
) -> NodeRelease
```

#### Описание

Функция возвращает шарнир по индексу узла.

#### Параметры

- nodeId (int): индекс узла,
для которого необходимо получить информацию о шарнире.

#### Возвращаемое значение

- NodeRelease: Шарнир в узле.

### getRodRelease

```python
getRodRelease(
  nodeId: int,
  rodId: int,
) -> RodRelease
```

#### Описание

Функция возвращает освобождения связи стержня по индексам узла и стержня.

#### Параметры

- nodeId (int): индекс узла, к которому привязан стержень.
- rodId (int): индекс стержня,
для которого необходимо получить информацию об освобождении связей.

#### Возвращаемое значение

- RodRelease: Освобождение связи стержня.
