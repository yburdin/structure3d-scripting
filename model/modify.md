# Редактирование конечных элементов

- [Операции редактирования КЭ](#операции-редактирования-кэ)
  - [splitRodsByDivision](#splitrodsbydivision)
  - [splitRodsByNodes](#splitrodsbynodes)
  - [extrude](#extrude)
  - [rotate](#rotate)
  - [mirror](#mirror)
  - [translateNodes](#translatenodes)
  - [mergeNodes](#mergenodes)
  - [delete](#delete)
- [Параметры редактирования](#параметры-редактирования)

## Операции редактирования КЭ

### splitRodsByDivision

```python
splitRodsByDivision(
  rodIds: list[int],
  numberOfDivision: int,
) -> list[int]
```

#### Описание

Функция разделяет стержни на указанное количество частей.

#### Параметры

- rodIds (list[int]): Список индексов стержней, которые необходимо разделить.
- numberOfDivision (int): Количество частей,
на которые следует разделить каждый из стержней.

#### Возвращаемое значение

- list[int]: Список индексов получившихся стержней.

### splitRodsByNodes

```python
splitRodsByNodes(
  rodIds: list[int],
) -> list[int]
```

#### Описание

Функция разделяет стержни с привязкой к существующим узлам.

#### Параметры

- rodIds (list[int]): Список индексов стержней, которые необходимо разделить.

#### Возвращаемое значение

- list[int]: Список индексов получившихся стержней.

### extrude

```python
extrude(
  objectIds: list[tuple[Enum, int]],
  direction: Coordinates,
  params: ExtrudeParams | None,
) -> list[tuple[Enum, int]]
```

#### Описание

Функция создает объекты путем выталкивания исходных объектов.

#### Параметры

- objectIds (list[tuple[Enum, int]]): Список кортежей,
содержащих индексы исходных объектов.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.
- direction (Coordinates): Направление выталкивание.
- params (ExtrudeParams | None): Дополнительные параметры выталкивания (необязательный).
Если не указаны, используются значения по умолчанию.

#### Возвращаемое значение

- list[tuple[Enum, int]]: Список кортежей,
содержащих индексы созданных вытянутых объектов.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.

### rotate

```python
rotate(
  objectIds: list[tuple[Enum, int]],
  axis: Axis,
  angle: float,
  createCopy: bool,
) -> list[tuple[Enum, int]] | None
```

#### Описание

Функция выполняет поворот указанных объектов вокруг заданной оси на указанный угол.
Поворот может быть выполнен с созданием копий объектов.

#### Параметры

- objectIds (list[tuple[Enum, int]]): Список кортежей,
содержащих индексы объектов, которые требуется повернуть.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.
- axis (Axis): Ось вращения.
- angle (float): Угол поворота в градусах.
- createCopy (bool): Флаг, указывающий нужно ли создавать копии повернутых объектов.

#### Возвращаемое значение

- list[tuple[Enum, int]] | None: Список кортежей,
содержащих индексы повернутых объектов.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.
Если createCopy установлен в True, возвращается список копий объектов.
В противном случае возвращается None.

### mirror

```python
mirror(
  objectIds: list[tuple[Enum, int]],
  mirrorPlane: Plane,
  createCopy: bool,
  params: MirrorParams | None,
) -> list[tuple[Enum, int]]
```

#### Описание

Функция выполняет отражение указанных объектов относительно заданной плоскости.
Отражение может быть выполнено с созданием копий объектов и
заданием дополнительных параметров.

#### Параметры

- objectIds (list[tuple[Enum, int]]): Список кортежей,
содержащих индексы объектов, которые требуется отразить.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.
- mirrorPlane (Plane): Плоскость отражения.
- createCopy (bool): Флаг, указывающий нужно ли создавать копии отраженных объектов.
- params (MirrorParams | None): Дополнительные параметры отражения (необязательный).
Если не указаны, используются значения по умолчанию.

#### Возвращаемое значение

- list[tuple[Enum, int]]: Список кортежей, содержащих индексы отраженных объектов.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.

### translateNodes

```python
translateNodes(
  nodeIds: list[int],
  direction: Coordinates,
) -> None
```

#### Описание

Функция выполняет смещение указанных узлов в заданном направлении.

#### Параметры

- nodeIds (list[int]): Список индексов узлов, которые требуется сместить.
- direction (Coordinates): Направление смещения.

### mergeNodes

```python
mergeNodes(
  gap: float,
) -> None
```

#### Описание

Функция выполняет совмещение узлов, находящихся на расстоянии менее заданного.

#### Параметры

- gap (float): Максимальный зазор между узлами.

### delete

```python
delete(
  objectIds: list[tuple[Enum, int]],
) -> None
```

#### Описание

Функция удаляет указанные объекты.

#### Параметры

- objectIds (list[tuple[Enum, int]]): Список кортежей,
содержащих индексы объектов, которые требуется удалить.
Каждый кортеж состоит из перечисления (Enum) и индекса объекта.

## Параметры редактирования

```python
class BaseModifyParams:
  duplicateBoundaryCondition: bool
```

```python
class ExtrudeParams(BaseModifyParams):  
  numberOfRepetitions: int
  scaleFactor: float
  twistAngle: float
  createSideRods: bool
  createSidePlates: bool
  createSolids: bool
```

```python
class MirrorParams(BaseModifyParams):
  ...
```

```python
class Axis:
  startPoint: Coordinates
  endPoint: Coordinates
```
