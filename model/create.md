# Добавление конечных элементов

- [Операции создания КЭ](#операции-создания-кэ)
  - [createNode](#createnode)
  - [createPointMass](#createpointmass)
  - [createRod](#createrod)
  - [createRodArc](#createrodarc)
  - [createElasticLink](#createelasticlink)
- [Освобождения связей](#освобождения-связей)
  - [addNodeRelease](#addnoderelease)
  - [addRodRelease](#addrodrelease)

## Операции создания КЭ

### createNode

```python
createNode(
  coordinates: Coordinates,
) -> int
```

#### Описание

Функция создает новый узел.

#### Параметры

- coordinates (Coordinates): Координаты нового узла.

#### Возвращаемое значение

- int: Индекс созданного узла.

### createPointMass

```python
createPointMass(
  nodeId: int,
) -> int
```

#### Описание

Функция создает новую точечную массу и присоединяет ее к указанному узлу.

#### Параметры

- nodeId (int): Индекс узла, к которому будет присоединена точечная масса.

#### Возвращаемое значение

- int: Индекс созданной точечной массы.

### createRod

```python
createRod(
  startNodeId: int,
  endNodeId: int,
) -> int
```

#### Описание

Функция создает новый стержень между двумя указанными узлами.

#### Параметры

- startNodeId (int): Индекс начального узла стержня.
- endNodeId (int): Индекс конечного узла стержня.

#### Возвращаемое значение

- int: Индекс созданного стержня.

### createRodArc

```python
createRodArc(
  startNodeId: int,
  endNodeId: int,
  centerNodeId: int,
  numberOfDivision: int,
) -> list[int]
```

#### Описание

Функция создает дугу из стержней, соединяющую начальный и конечный узлы,
с центром в указанном узле.

#### Параметры

- startNodeId (int): Индекс начального узла дуги стержня.
- endNodeId (int): Индекс конечного узла дуги стержня.
- centerNodeId (int): Индекс узла - центра дуги.
- numberOfDivision (int): Количество стержней, на которое разбивается дуга.

#### Возвращаемое значение

- list[int]: Список индексов созданных стержней.

### createElasticLink

```python
createElasticLink(
  startNodeId: int,
  endNodeId: int,  
) -> int
```

#### Описание

Функция создает упругую связь между двумя узлами.

#### Параметры

- startNodeId (int): Индекс начального узла связи.
- endNodeId (int): Индекс конечного узла связи.

#### Возвращаемое значение

- int: Индекс созданной связи.

## Освобождения связей

### addNodeRelease

```python
addNodeRelease(
  nodeId: int,
  release: NodeRelease,
) -> None
```

#### Описание

Функция добавляет шарнир в указанный узел.

#### Параметры

- nodeId (int): Индекс узла, в который необходимо добавить шарнир.
- release (NodeRelease): Объект, содержащий информацию об освобожденных связях.

### addRodRelease

```python
addRodRelease(
  rodId: int,
  nodeId: int,
  release: RodRelease,
) -> None
```

#### Описание

Функция добавляет освобождение связи указанного стержня в указанном узле.

#### Параметры

- rodId (int): Индекс стержня, для которого добавляется освобождение связи.
- nodeId (int): Индекс узла, в котором освобождается связь.
- release (RodRelease): Объект, содержащий информацию об освобожденных связях.

## Базовые классы

```python
class Coordinates:
  x: float
  y: float
  z: float
```

```python
class NodeRelease:
  releaseUX: bool
  releaseUY: bool
  releaseUZ: bool
```

```python
class RodRelease(NodeRelease):
  releaseRX: bool
  releaseRY: bool
  releaseRZ: bool
```
